<?php

namespace App\MyService;


class Btn {

    public function a($url,$text=null){

        return view('btn.a',[

            'url' => url($url),
            'text' => is_null($text) ? $url : $text

        ]);

    }

    public function button($text,$css=null){


        return view('btn.button',[

            'text' => $text,
            'css' => $css

        ]);

    }




}