<?php

namespace App\Providers;

use App\MyService\Btn;
use Illuminate\Support\ServiceProvider;


class BtnServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('btn', Btn::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
