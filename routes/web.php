<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\MyController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\MyService\Facades\Btn;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('customers',CustomerController::class);

Route::get('generate-pdf/{customer}', [CustomerController::class, 'generatepdf']);

//CSV

Route::get('importExportView', [MyController::class, 'importExportView']);
Route::get('export', [MyController::class, 'export'])->name('export');
Route::post('import', [MyController::class, 'import'])->name('import');
Route::get('myservice', function(){

    return Btn::a('about','Acerca de...');

});

Route::get('genBtn',function(){


    return Btn::button('Login', 'background-color:red;border:5px solid black;text-decoration: none');

});
